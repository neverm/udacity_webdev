from common.session_handler import SessionHandler
from models.user import User
import re

USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
PASS_RE = re.compile(r"^.{3,20}$")
EMAIL_RE = re.compile(r"^[\S]+@[\S]+.[\S]+$")

def valid_username(username):
    return USER_RE.match(username)

def valid_pass(password):
    return USER_RE.match(password)

def valid_email(email):
    return EMAIL_RE.match(email)

class AuthHandler(SessionHandler):
    "Handler that holds together some common operations for authentication"
    def login(self, user):
        user_id = user.key().id()
        self.set_cookie("user_id", str(user_id))

    def logout(self):
        self.delete_cookie("user_id")

class SignUpHandler(AuthHandler):

    def get(self):
        self.render("signup.html")

    def post(self):
        username = self.request.get('username')
        password = self.request.get('password')
        verify = self.request.get('verify')
        email = self.request.get('email')
        invalid = False
        params = {"username": username, "verify": verify, "email": email}
        if not valid_username(username):
            params["username_error"] = "Thts not a valid username"
            invalid = True
        elif User.by_name(username):
            params["general_error"] = "username already exists"
            invalid = True
        if not valid_pass(password):
            params["password_error"] = "Thts not a valid password"
            invalid = True
        elif not verify or verify != password:
            params["verify_error"] = "Passwords do not match"
            invalid = True
        if email and not valid_email(email):
            params["email_error"] = "Thts not a valid email"
            invalid = True
        if invalid:
            self.render("signup.html", **params)
        else:
            user = User.register(username, password, email)
            self.login(user)
            self.redirect('/')


class LoginHandler(AuthHandler):

    def get(self):
        self.render("login.html")

    def post(self):
        username = self.request.get("username")
        password = self.request.get("password")
        if username and password:
            user = User.login(username, password)
            if user:
                self.login(user)
                # todo: redirect to referer if it's in our domain
                self.redirect('/')
                return
        self.render("login.html", error="Invalid login")


class LogoutHandler(AuthHandler):

    def get(self):
        self.logout()
        self.redirect('/')
