from common.session_handler import SessionHandler
from definitions import EDIT_PAGE, HISTORY_PAGE
from models.page import Page
from collections import namedtuple
import logging
import time


Action = namedtuple('Action', 'url action_name')

def make_action_links(page_name, except_action):
    all_actions = [Action("/_edit/"+page_name, "edit"),
                   Action("/"+page_name, "view"),
                   Action("/_history/"+page_name, "history")]
    return [a for a in all_actions if a.action_name != except_action]


class PageHandler(SessionHandler):

    def get(self, page_name):
        actions = make_action_links(page_name, "view")
        if page_name == EDIT_PAGE or page_name == HISTORY_PAGE:
            self.redirect("/")
            return
        version = self.request.get("v")
        if version:
            page = Page.by_name_version(page_name, version)
        else:
            page = Page.by_name(page_name)
        if page:
            self.render("page.html", page_content=page.content, page_name=page.name, actions=actions)
        else:
            if version:
                self.redirect("/" + page_name)
            else:
                self.redirect("/" + EDIT_PAGE + "/" + page_name)


class EditHandler(SessionHandler):

    def get(self, page_name):
        if not self.user:
            self.redirect("/login")
        # check if page with such name exists
        page = Page.by_name(page_name)
        if not page:
            self.render("edit.html")
        else:
            actions = make_action_links(page_name, "edit")
            self.render("edit.html", content=page.content, actions=actions)

    def post(self, page_name):
        content = self.request.get("content")
        page = Page.by_name(page_name)
        if content:
            if not page:
                page = Page(name=page_name, content=content, version=1)
            else:
                page = Page(name=page_name, content=content,
                    version=page.version+1)
            page.put()
            time.sleep(.5)
            self.redirect("/" + page.name)
        else:
            error = "Content can not be empty"
            if page:
                self.render("edit.html", content=content, error=error)
            else:
                actions = make_action_links(page_name, "edit")
                self.render("edit.html", content=content, error=error, actions=actions)


class HistoryHandler(SessionHandler):

    def get(self, page_name):
        if page_name == EDIT_PAGE or page_name == HISTORY_PAGE:
            self.redirect("/")
            return
        versions = list(Page.by_name_all_versions(page_name))
        if versions:
            actions = make_action_links(page_name, "history")
            self.render("history.html", actions=actions, page_name=page_name, versions=versions)
        else:
            self.redirect("/" + EDIT_PAGE + "/" + page_name)