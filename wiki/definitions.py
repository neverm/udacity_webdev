"""
Global constants defined for the whole project
"""

import os


ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
TEMPLATES_DIR_NAME = "templates"
TEMPLATES_DIR = os.path.join(ROOT_DIR, TEMPLATES_DIR_NAME)
FRONT_PAGE_NAME = "front_page"
EDIT_PAGE = "_edit"
HISTORY_PAGE = "_history"
