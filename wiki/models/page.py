from google.appengine.ext import db


class Page(db.Model):
    name = db.StringProperty(required=True)
    content = db.TextProperty(required=True)
    version = db.IntegerProperty(required=True)
    registered_at = db.DateTimeProperty(auto_now_add=True)

    # @classmethod
    # def by_id(cls, page_id):
    #     return Page.get_by_id(int(page_id))

    @classmethod
    def by_name(cls, page_name):
        page = cls.by_name_all_versions(page_name).get()
        return page

    @classmethod
    def by_name_version(cls, page_name, version):
        if version.isdigit():
            version = int(version)
            page = cls.by_name_all_versions(page_name).filter('version =', version).get()
            return page

    @classmethod
    def by_name_all_versions(cls, page_name):
        all_versions = cls.all().filter('name =', page_name).order('-version')
        return all_versions
