from google.appengine.ext import db
from common.hashing import hash_password, check_password


class User(db.Model):
    username = db.StringProperty(required=True)
    hashed_password = db.StringProperty(required=True)
    email = db.StringProperty()
    registered_at = db.DateTimeProperty(auto_now_add=True)

    @classmethod
    def by_id(cls, user_id):
        return User.get_by_id(int(user_id))

    @classmethod
    def by_name(cls, username):
        u = User.all().filter('username =', username).get()
        return u

    @classmethod
    def register(cls, username, password, email=None):
        hashed_password = hash_password(username, password)
        user = User(username=username, hashed_password=hashed_password, email=email)
        user.put()
        return user

    @classmethod
    def login(cls, username, password):
        user = cls.by_name(username)
        if user and check_password(username, password, user.hashed_password):
            return user
