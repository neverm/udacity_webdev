"""
Generic cookie classes and helper functions, used to simplify
cookie encryption and decryption
"""

import hmac

SECRET = "zAJyOHzQDmhvCGfivsJQSzGnyfLKSjFU"
KEY_VALUE_SEPARATOR = "="
HASH_SEPARATOR = "|"

def hash_str(s, secret):
    return hmac.new(secret, s).hexdigest()

def secure_str(s):
    return s + HASH_SEPARATOR + hash_str(s, SECRET)

def unsecure_str(secured_str):
    """Given secured string in form unsecured|hash try to get unsecured part
    of it. In case the string is malformed, or the hash does not correspond
    to hashing unsecured part with current secret return None"""
    split = secured_str.split(HASH_SEPARATOR)
    if len(split) >= 2: # HASH_SEPARATOR may be also found in string itself
        unsecured = "".join(split[:-1]) # get everything except for the hash
        h = split[-1]
        if secure_str(unsecured) == secured_str:
            return unsecured

class Cookie(object):

    def __init__(self, key, value):
        self.key = key
        self.value = value

    @classmethod
    def from_secure_val(cls, key, secure_value):
        val = unsecure_str(secure_value)
        if val:
            return cls(key, val)
        else:
            return None

    def as_secure_str(self):
        "get secure string that is ready to be sent in set-cookie header"
        return self.key + KEY_VALUE_SEPARATOR + secure_str(self.value)

    def as_str(self):
        "get key-value text representation of this cookie"
        return self.key + KEY_VALUE_SEPARATOR + self.value