"""
Basic handler extension that simplifies rendering templates
"""
import webapp2
import os
import jinja2
from cookie import Cookie
from definitions import TEMPLATES_DIR

class BaseHandler(webapp2.RequestHandler):
    "Extension of a webapp2 handler that uses template renderer to produce output"

    def initialize(self, *a, **kw):
        webapp2.RequestHandler.initialize(self, *a, **kw)
        self.template_args = dict()
        self.jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(TEMPLATES_DIR),
            autoescape=True)

    def add_template_args(self, override=False, **template_args):
        if override:
            self.template_args = dict(self.template_args, **template_args)
        else:
            self.template_args = dict(template_args, **self.template_args)

    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render(self, template_name, **kw):
        "render template specified by template_name"
        # always override present template args with the ones passed as arguments
        args = dict(self.template_args, **kw)
        t = self.jinja_env.get_template(template_name)
        self.write(t.render(**args))

    def delete_cookie(self, key, path="/"):
        "delete given cookie by setting it's value to empty string"
        self.response.headers.add_header('Set-Cookie', "%s=;Path=%s" % (key, path))

    def get_cookie_value(self, key, default=None):
        """get strinig value of the cookie identified by this key that
        was previously created with set_cookie function (and thus is 
        secured with hashing)
        return default if cookie by such key can't be find or hashing part is invalid
        """
        cookie_raw_val = self.request.cookies.get(key)
        if cookie_raw_val:
            cookie = Cookie.from_secure_val(key, cookie_raw_val)
            if cookie:
                return cookie.value
        return default

    def set_cookie(self, key, value, path="/"):
        """Add Set-Cookie header to the response, and assign a key-value pair
        joined by '=' character and secured by hashing.
        """
        cookie_str = Cookie(key, value).as_secure_str()
        cookie_str += "; Path=" + path
        self.response.headers.add_header('Set-Cookie', cookie_str)
