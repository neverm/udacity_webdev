import hmac
import random
import string
import hashlib

def generate_salt():
    return "".join([random.choice(string.letters) for x in range(10)])

def hash_password(username, password, salt=None):
    """generate hashed of password, together with a generated salt
    string, separated by comma "," and attached to the end of it"""
    if not salt:
        salt = generate_salt()
    hashed = hashlib.sha256(username+password+salt).hexdigest()
    return "%s,%s" % (hashed, salt)

def check_password(username, password, hashed_password):
    """return true if given password is correct for given username
    based on hashed_password value"""
    if not "," in hashed_password:
        return False
    salt = hashed_password.split(",")[1]
    return hash_password(username, password, salt) == hashed_password
    