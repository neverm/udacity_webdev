from base_handler import BaseHandler
from models.user import User

class SessionHandler(BaseHandler):
    """
    Hander that on top of template rendering keeps track
    of current user session. If user cookie is set and valid
    it sets user field during initialization which can later
    be used to check whether user has logged in
    """
    def initialize(self, *a, **kw):
        BaseHandler.initialize(self, *a, **kw)
        uid = self.get_cookie_value("user_id")
        self.user = None
        if uid:
            self.user = User.by_id(uid)
            if self.user:
                self.add_template_args(user=self.user)
