# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import webapp2
from common.session_handler import SessionHandler
from pages.authentication import SignUpHandler, LoginHandler, LogoutHandler
from pages.wikipage import PageHandler, EditHandler, HistoryHandler
from definitions import EDIT_PAGE, FRONT_PAGE_NAME, HISTORY_PAGE

PAGE_RE = r'/((?:[a-zA-Z0-9_-]+/?)*)'

class RootHandler(SessionHandler):
     
    def get(self):
        self.redirect("/" + FRONT_PAGE_NAME)

app = webapp2.WSGIApplication([
    (r'/', RootHandler),
    (r'/signup/?', SignUpHandler),
    (r'/login/?', LoginHandler),
    (r'/logout/?', LogoutHandler),
    ('/'+ EDIT_PAGE + PAGE_RE, EditHandler),
    ('/'+ HISTORY_PAGE + PAGE_RE, HistoryHandler),
    (PAGE_RE, PageHandler)
], debug=True)
