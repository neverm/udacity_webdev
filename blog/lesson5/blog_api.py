from lesson3.blog import Post
from common.handler import BaseHandler
import json

def post_to_dict(post):
        json_dict = {}
        json_dict['content'] = post.content
        json_dict['subject'] = post.subject
        json_dict['created'] = post.created.strftime("%c")
        return json_dict


class JsonHandler(BaseHandler):

    def write_json(self, data):
        self.response.headers['Content-Type'] = 'application/json'
        self.write(json.dumps(data))


class BlogJsonHandler(JsonHandler):

    def get(self):
        posts = Post.get_all()
        posts_dict_list = [post_to_dict(post) for post in posts]
        self.write_json(posts_dict_list)


class PostJsonHandler(JsonHandler):

    def get(self, post_id):
        post = Post.by_id(post_id)
        if not post:
            self.error(404)
            return
        self.write_json(post_to_dict(post))
