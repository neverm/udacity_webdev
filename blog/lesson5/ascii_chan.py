from common.handler import Renderer, BaseHandler
from google.appengine.ext import db
import urllib2
import json

renderer = Renderer(__file__)

class Handler(BaseHandler):

    def render(self, template, **kw):
        self.render_template(renderer, template, **kw)

GMAPS_URL2 = "https://maps.googleapis.com/maps/api/staticmap?size=600x300&maptype=roadmap%s&key=AIzaSyA1GYbimooXfKCrydncsSuzZCeM1DzkocQ"

ip_coord_url = 'http://ip-api.com/json/'
def get_coords(ip):
    ip = "4.2.2.2"
    url = ip_coord_url + ip
    content = urllib2.urlopen(url).read()
    if content:
        json_str = json.loads(content)
        lat = json_str.get("lat", None)
        lon = json_str.get("lon", None)
        if lat and lon:
            return db.GeoPt(lat, lon)

def gmap_img(points):
    markers = "".join(["&markers=%s,%s" % (p.lat, p.lon) for p in points])
    return GMAPS_URL2 % markers

class GeoArt(db.Model):
    title = db.StringProperty(required=True)
    art = db.TextProperty(required=True)
    created = db.DateTimeProperty(auto_now_add=True)
    coords = db.GeoPtProperty()


class AsciiGeoPage(Handler):

    def render_front(self, title="", art="", error="", arts=None, img_url=None):
        # self.write(repr(get_coords(self.request.remote_addr)))
        self.render("ascii_chan.html", title=title, art=art, error=error, arts=arts, img_url=img_url)

    def get(self):
        arts = db.GqlQuery("SELECT * FROM GeoArt ORDER BY created DESC")
        # prevent running the same query multiple times
        arts = list(arts)
        geo_points = [art.coords for art in arts if art.coords]
        img_url = None
        if geo_points:
            img_url = gmap_img(geo_points)
        self.render_front(arts=arts, img_url=img_url)

    def post(self):
        title = self.request.get("title")
        art = self.request.get("art")
        if title and art:
            art = GeoArt(title=title, art=art)
            coords = get_coords(self.request.remote_addr)
            if coords:
                art.coords = coords
            art.put()
            self.redirect("/lesson5/ascii")
        else:
            error = "Both fields must be filled"
            self.render_front(title, art, error)
