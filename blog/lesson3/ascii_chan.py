from common.handler import Renderer, BaseHandler
from google.appengine.ext import db

renderer = Renderer(__file__)

class Handler(BaseHandler):

    def render(self, template, **kw):
        self.render_template(renderer, template, **kw)


class Art(db.Model):
    title = db.StringProperty(required=True)
    art = db.TextProperty(required=True)
    created = db.DateTimeProperty(auto_now_add=True)


class AsciiPage(Handler):

    def render_front(self, title="", art="", error="", arts=None):
        self.render("ascii_chan.html", title=title, art=art, error=error, arts=arts)

    def get(self):
        arts = db.GqlQuery("SELECT * FROM Art ORDER BY created DESC")
        self.render_front(arts=arts)

    def post(self):
        title = self.request.get("title")
        art = self.request.get("art")
        if title and art:
            a = Art(title=title, art=art)
            a.put()
            self.redirect("/lesson3/ascii")
        else:
            error = "Both fields must be filled"
            self.render_front(title, art, error)
