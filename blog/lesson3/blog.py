from common.handler import Renderer, BaseHandler
from google.appengine.ext import db
import logging

DEBUG = True

renderer = Renderer(__file__)

class Handler(BaseHandler):

    def render(self, template, **kw):
        self.render_template(renderer, template, **kw)


class Post(db.Model):
    subject = db.StringProperty(required=True)
    content = db.TextProperty(required=True)
    created = db.DateTimeProperty(auto_now_add=True)

    @classmethod
    def by_id(cls, post_id):
        return cls.get_by_id(int(post_id))

    @classmethod
    def create(cls, subject, content):
        p = Post(subject=subject, content=content)
        p.put()
        return p

    @classmethod
    def get_all(cls):
        return cls.all()


class SinglePostPage(Handler):

    def get(self, post_id):
        post = Post.by_id(post_id)
        if not post:
            self.error(404)
            return
        self.render("blog_single_post.html", post=post)


class NewPostHandler(Handler):

    def render_front(self, subject="", content=""):
        self.render("newpost.html", subject=subject, content=content)

    def get(self):
        self.render_front()

    def post(self):
        subject = self.request.get("subject")
        content = self.request.get("content")
        if subject and content:
            p = Post.create(str(subject), str(content))
            post_id = p.key().id()
            self.redirect("/blog/" + str(post_id))
        else:
            error = "Subject and content should be nonempty"
            self.render_front(subject, content, error)


class BlogPage(Handler):

    def get(self):
        posts = Post.get_all()
        logging.debug("sraka\n\n\n\n\n: ")
        self.render("blog_front.html", posts=posts)
