# PSET 2 Quizz 1

import webapp2
import cgi
import string

rot_html = """
<h1>Enter some text to ROT13</h1>
<form method="post">
<textarea style="width: 500px" name="text" rows="10">%s</textarea>
<br>
<input type="submit">
</form>
"""

def rot13_rotate_char(char):
    if char in string.ascii_lowercase:
        alphabet = string.ascii_lowercase
    elif char in string.ascii_uppercase:
        alphabet = string.ascii_uppercase
    else:
        return char
    idx = alphabet.index(char)
    return alphabet[(idx+13) % len(alphabet)]

def rot13_rotate_text(text):
    return "".join([rot13_rotate_char(c) for c in list(text)])

class RotPage(webapp2.RequestHandler):

    def get(self):
        self.response.write(rot_html % "")

    def post(self):
        text = self.request.get('text')
        rotated = rot13_rotate_text(text)
        self.response.write(rot_html % cgi.escape(rotated, quote=True))