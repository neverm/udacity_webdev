# PSET 2 Quizz 2

import webapp2
import cgi
import re
from collections import defaultdict

signup_html = """
<h1>Signup</h1>
<form method="POST">

<label>Username<input type="text" name="username" value="%(username)s"></label>
<span>%(username_error)s</span><br>

<label>Password<input type="password" name="password" value=""></label>
<span>%(password_error)s</span><br>

<label>Verify password<input type="password" name="verify"></label>
<span>%(verify_error)s</span><br>

<label>Email<input type="text" name="email" value="%(email)s"></label>
<span>%(email_error)s</span><br>

<input type="submit">

</form>
"""

signup_success_html = """
Welcome, %s!
"""

USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
PASS_RE = re.compile(r"^.{3,20}$")
EMAIL_RE = re.compile(r"^[\S]+@[\S]+.[\S]+$")

def valid_username(username):
    return USER_RE.match(username)

def valid_pass(password):
    return USER_RE.match(password)

def valid_email(email):
    return EMAIL_RE.match(email)

class SignUpPage(webapp2.RequestHandler):

    def print_form(self, params):
        params = { k: cgi.escape(v, quote=True) for k, v in params.items()}
        params = defaultdict(lambda: "", params)
        self.response.write(signup_html % params)

    def get(self):
        self.print_form(dict())

    def post(self):
        username = self.request.get('username')
        password = self.request.get('password')
        verify = self.request.get('verify')
        email = self.request.get('email')
        invalid = False
        params = {"username": username, "verify": verify, "email": email}
        if not valid_username(username):
            params["username_error"] = "Thts not a valid username"
            invalid = True
        if not valid_pass(password):
            params["password_error"] = "Thts not a valid password"
            invalid = True
        elif not verify or verify != password:
            params["verify_error"] = "Passwords do not match"
            invalid = True
        if email and not valid_email(email):
            params["email_error"] = "Thts not a valid email"
            invalid = True
        if invalid:
            self.print_form(params)
        else:
            self.redirect('/lesson2/signup-success?username='+username)

class SignUpSuccessPage(webapp2.RequestHandler):

    def get(self):
        username = self.request.get("username")
        self.response.write(signup_success_html % username)