from common.handler import Renderer, BaseHandler


renderer = Renderer(__file__)

class Handler(BaseHandler):

    def render(self, template, **kw):
        self.render_template(renderer, template, **kw)

class TemplateTestPage(Handler):

    def get(self):
        items = self.request.get_all("food")
        self.render("shopping_list.html", items=items, title="Shopping List")

class TemplateAboutPage(Handler):

    def get(self):
        self.render("about.html", title="About Us")