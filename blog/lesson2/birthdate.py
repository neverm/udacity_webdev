# lesson 2 lectures

import webapp2
import cgi
import re

months = ['January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December']
month_abbr = {month[:3].lower(): month for month in months}


form = """
<form method="post">
    what is you b-day?
    <br>
    <label> Day
        <input type="text" name="day" value="%(day)s">
    </label>
    <br>

    <label> Month
    <input type="text" name="month" value="%(month)s">
    </label>
    <br>

    <label> Year
    <input type="text" name="year" value="%(year)s">
    </label>
    <div style="color: red">%(error)s</div>
    <br>
    <br><br>
    <input type="submit">
</form>
"""


def get_valid_month(user_month_string):
    if (len(user_month_string) >= 3):
        return month_abbr.get(user_month_string.lower()[:3])

def get_valid_day(day):
    if (day.isdigit()):
        day_num = int(day)
        if (day_num > 0 and day_num <= 31):
            return day_num

def get_valid_year(year):
    if (year.isdigit()):
        year_num = int(year)
        if (year_num >= 1900 and year_num <= 2020):
            return year_num
        

class EnterBirthdayPage(webapp2.RequestHandler):

    def write_escapted_params(self, string, dict_params):
        params = {k: cgi.escape(v, quote=True) for k, v in dict_params.items()}
        self.response.write(string % params)

    def write_form(self, error="", month="", day="", year=""):
        self.write_escapted_params(form, {"error": error,
                                          "month": month,
                                          "day": day,
                                          "year": year,})

    def get(self):
        self.response.headers['Content-Type'] = 'text/html'
        self.write_form()

    def post(self):
        user_month = self.request.get('month')
        user_day = self.request.get('day')
        user_year = self.request.get('year')

        if not (get_valid_year(user_year) and 
            get_valid_day(user_day) and get_valid_month(user_month)):
            self.write_form("Congratulations! You are pidor now", user_month,
                user_day, user_year)
        else:
            self.redirect("/lesson2/date-success")

class ThanksPage(webapp2.RequestHandler):

    def get(self):
        self.response.write("Thts valid merci")