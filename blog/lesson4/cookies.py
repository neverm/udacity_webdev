from common.handler import Renderer, BaseHandler
from google.appengine.ext import db
import hmac

renderer = Renderer(__file__)

class Handler(BaseHandler):

    def render(self, template, **kw):
        self.render_template(renderer, template, **kw)


class CookiesHandler(Handler):

    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        visits = int(self.get_cookie_value("visits", 0))
        visits += 1
        self.set_cookie("visits", str(visits))
        self.write("You've been here %s times" % visits)
