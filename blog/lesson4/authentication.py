# PSET 4 Quizz 1

import re
import webapp2
from google.appengine.ext import db
from common.handler import Renderer, BaseHandler
from common.hashing import hash_password, check_password

renderer = Renderer(__file__)

class Handler(BaseHandler):

    def render(self, template, **kw):
        self.render_template(renderer, template, **kw)


signup_success_html = """
Welcome, %s!
"""

USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
PASS_RE = re.compile(r"^.{3,20}$")
EMAIL_RE = re.compile(r"^[\S]+@[\S]+.[\S]+$")

def valid_username(username):
    return USER_RE.match(username)

def valid_pass(password):
    return USER_RE.match(password)

def valid_email(email):
    return EMAIL_RE.match(email)


class UserSessionHandler(Handler):

    def login(self, user):
        user_id = user.key().id()
        self.set_cookie("user_id", str(user_id))

    def logout(self):
        self.delete_cookie("user_id")

    def initialize(self, *a, **kw):
        webapp2.RequestHandler.initialize(self, *a, **kw)
        uid = self.get_cookie_value("user_id")
        if uid:
            self.user = User.by_id(uid)


class SignUpHandler(UserSessionHandler):

    def get(self):
        self.render("signup.html")

    def post(self):
        username = self.request.get('username')
        password = self.request.get('password')
        verify = self.request.get('verify')
        email = self.request.get('email')
        invalid = False
        params = {"username": username, "verify": verify, "email": email}
        if not valid_username(username):
            params["username_error"] = "Thts not a valid username"
            invalid = True
        elif User.by_name(username):
            params["general_error"] = "username already exists"
            invalid = True
        if not valid_pass(password):
            params["password_error"] = "Thts not a valid password"
            invalid = True
        elif not verify or verify != password:
            params["verify_error"] = "Passwords do not match"
            invalid = True
        if email and not valid_email(email):
            params["email_error"] = "Thts not a valid email"
            invalid = True
        if invalid:
            self.render("signup.html", **params)
        else:
            user = User.register(username, password, email)
            self.login(user)
            self.redirect('/blog/welcome')


class WelcomeHandler(UserSessionHandler):

    def get(self):
        if not self.user:
            self.redirect("/blog/signup")
            return
        self.write(signup_success_html % self.user.username)


class LoginHandler(UserSessionHandler):

    def get(self):
        self.render("login.html")

    def post(self):
        username = self.request.get("username")
        password = self.request.get("password")
        if username and password:
            user = User.login(username, password)
            if user:
                self.login(user)
                self.redirect('/blog/welcome')
                return
        self.render("login.html", error="Invalid login")


class LogoutHandler(UserSessionHandler):

    def get(self):
        self.logout()
        self.redirect('/blog/signup')


class User(db.Model):
    username = db.StringProperty(required=True)
    hashed_password = db.StringProperty(required=True)
    email = db.StringProperty()
    registered_at = db.DateTimeProperty(auto_now_add=True)

    @classmethod
    def by_id(cls, user_id):
        return User.get_by_id(int(user_id))

    @classmethod
    def by_name(cls, username):
        u = User.all().filter('username =', username).get()
        return u

    @classmethod
    def register(cls, username, password, email=None):
        hashed_password = hash_password(username, password)
        user = User(username=username, hashed_password=hashed_password, email=email)
        user.put()
        return user

    @classmethod
    def login(cls, username, password):
        user = cls.by_name(username)
        if user and check_password(username, password, user.hashed_password):
            return user

