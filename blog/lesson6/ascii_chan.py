from common.handler import Renderer, BaseHandler
from google.appengine.ext import db
from google.appengine.api import memcache
import urllib2
import json
import logging
import time
import webapp2

renderer = Renderer(__file__)

class Handler(BaseHandler):

    def render(self, template, **kw):
        self.render_template(renderer, template, **kw)

GMAPS_URL2 = "https://maps.googleapis.com/maps/api/staticmap?size=600x300&maptype=roadmap%s&key=AIzaSyA1GYbimooXfKCrydncsSuzZCeM1DzkocQ"

ip_coord_url = 'http://ip-api.com/json/'
def get_coords(ip):
    url = ip_coord_url + ip
    content = urllib2.urlopen(url).read()
    if content:
        json_str = json.loads(content)
        lat = json_str.get("lat", None)
        lon = json_str.get("lon", None)
        if lat and lon:
            return db.GeoPt(lat, lon)

def gmap_img(points):
    markers = "".join(["&markers=%s,%s" % (p.lat, p.lon) for p in points])
    return GMAPS_URL2 % markers

def load_top_from_db():
    logging.error("DB READ")
    arts = db.GqlQuery("SELECT * FROM GeoArt ORDER BY created DESC")
    # prevent running the same query multiple times
    return list(arts)

def get_all_arts(cache, update=False):
    key = "top"
    cached = cache.get(key)
    if cached is None:
        arts = load_top_from_db()
        cache.set(key, arts)
    elif update:
        succeed = False
        while not succeed:
            arts = load_top_from_db()
            succeed = cache.cas(key, arts)
            cache.gets(key)
        return arts
    return cached

class GeoArt(db.Model):
    title = db.StringProperty(required=True)
    art = db.TextProperty(required=True)
    created = db.DateTimeProperty(auto_now_add=True)
    coords = db.GeoPtProperty()


class CachedAsciiPage(Handler):

    def initialize(self, *a, **kw):
        webapp2.RequestHandler.initialize(self, *a, **kw)
        self.cache = memcache.Client()

    def render_front(self, title="", art="", error="", arts=None, img_url=None):
        self.render("ascii_chan.html", title=title, art=art, error=error, arts=arts, img_url=img_url)

    def get(self):
        # time.sleep(3)
        arts = get_all_arts(self.cache)
        geo_points = [art.coords for art in arts if art.coords]
        img_url = None
        if geo_points:
            img_url = gmap_img(geo_points)
        self.render_front(arts=arts, img_url=img_url)

    def post(self):
        title = self.request.get("title")
        art = self.request.get("art")
        if title and art:
            art = GeoArt(title=title, art=art)
            coords = get_coords(self.request.remote_addr)
            if coords:
                art.coords = coords
            art.put()
            # because dumb app endgine may not update db immediately
            time.sleep(1)
            # update the cache
            get_all_arts(self.cache, True)
            self.redirect("/lesson6/ascii")
        else:
            error = "Both fields must be filled"
            self.render_front(title, art, error)
