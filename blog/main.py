# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import webapp2
from lesson2.rot13 import RotPage
from lesson2.signup import SignUpPage, SignUpSuccessPage
from lesson2.birthdate import EnterBirthdayPage, ThanksPage
from lesson2.templates import TemplateTestPage, TemplateAboutPage
from lesson3.ascii_chan import AsciiPage
from lesson3.blog import BlogPage, SinglePostPage, NewPostHandler
from lesson4.cookies import CookiesHandler
from lesson4.authentication import SignUpHandler, WelcomeHandler, LoginHandler, LogoutHandler
from lesson5.ascii_chan import AsciiGeoPage
from lesson5.blog_api import BlogJsonHandler, PostJsonHandler
from lesson6.ascii_chan import CachedAsciiPage


app = webapp2.WSGIApplication([
    (r'/lesson2/?', EnterBirthdayPage),
    (r'/lesson2/date-success/?', ThanksPage),
    (r'/lesson2/rot13/?', RotPage),
    (r'/lesson2/signup/?', SignUpPage),
    (r'/lesson2/signup-success/?', SignUpSuccessPage),
    (r'/lesson2/templates/?', TemplateTestPage),
    (r'/lesson2/templates/about/?', TemplateAboutPage),
    (r'/lesson3/ascii/?', AsciiPage),
    (r'/lesson4/cookies/?', CookiesHandler),
    (r'/lesson5/ascii/?', AsciiGeoPage),
    (r'/lesson6/ascii/?', CachedAsciiPage),

    # blog psets are under the same url, but are physically in different lesson folders
    (r'/blog/?', BlogPage),
    (r'/blog/(\d+)/?', SinglePostPage),
    (r'/blog/newpost/?', NewPostHandler),
    (r'/blog/\.json/?', BlogJsonHandler),
    (r'/blog/(\d+)\.json/?', PostJsonHandler),
    (r'/blog/signup/?', SignUpHandler),
    (r'/blog/welcome/?', WelcomeHandler),
    (r'/blog/login/?', LoginHandler),
    (r'/blog/logout/?', LogoutHandler),

], debug=True)
