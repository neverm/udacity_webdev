"""
Basic handler extension that simplifies rendering templates
"""
import webapp2
import os
import jinja2
from cookie import Cookie


class BaseHandler(webapp2.RequestHandler):
    "Extension of a webapp2 handler that uses template renderer to produce output"
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_template(self, renderer, template, **kw):
        "render given template against given renderer and respond with the result"
        self.write(renderer.render(template, **kw))

    def delete_cookie(self, key, path="/"):
        "delete given cookie by setting it's value to empty string"
        self.response.headers.add_header('Set-Cookie', "%s=;Path=%s" % (key, path))

    def get_cookie_value(self, key, default=None):
        """get strinig value of the cookie identified by this key that
        was previously created with set_cookie function (and thus is 
        secured with hashing)
        return default if cookie by such key can't be find or hashing part is invalid
        """
        cookie_raw_val = self.request.cookies.get(key)
        if cookie_raw_val:
            cookie = Cookie.from_secure_val(key, cookie_raw_val)
            if cookie:
                return cookie.value
        return default

    def set_cookie(self, key, value, path="/"):
        """Add Set-Cookie header to the response, and assign a key-value pair
        joined by '=' character and secured by hashing.
        """
        cookie_str = Cookie(key, value).as_secure_str()
        cookie_str += "; Path=" + path
        self.response.headers.add_header('Set-Cookie', cookie_str)


class Renderer(object):

    def __init__(self, current_file, templatesFolder="templates"):
        self.jinja_env = self.get_jinja_env(current_file, templatesFolder)

    def get_jinja_env(self, current_file, templatesFolder):
        """init jinja environment with templates folder located with respect
        to current_file, specified by the caller (should pass __file__ there)"""
        template_dir = os.path.join(os.path.dirname(current_file), templatesFolder)
        jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir),
            autoescape=True)
        return jinja_env

    def render(self, template, **kw):
        t = self.jinja_env.get_template(template)
        return t.render(kw)
